# Tools for work
1.  VS Code
    1.  VS Code Microsoft Docker extension
    2.  VS Code Terraform extension
2.  Docker
    1.  Docker Desktop
        1.  Docker version 20.10.2, build 2291f61
3.  Google Chrome ModHeader extension
4.  Gitlab CICD
    1.  git version 2.16.6
5.  GNU make 4.3
## NGINX Proxy
## WorkFlow
1.  Add new project in Gitlab for proxy application
2.  Create Dockerfile + NGINX configurations
3.  Setup AWS account with new ECR repo - Docker Hub
4.  Create a CI user account for Gitlab to autheticate with AWS
5.  Setup pipeline jobs in Gitlab to run workflow

    ![alt text](https://gitlab.com/alochym/recipe-app-api-proxy/-/raw/master/images/gitlab-work-flow.png)
